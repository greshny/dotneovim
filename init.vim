" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')
" ruby/rails related stuff {{
Plug 'vim-scripts/ruby-matchit', { 'for': 'ruby' }
Plug 'tpope/vim-rails',          { 'for': 'ruby' }
Plug 'tpope/vim-bundler',        { 'for': 'ruby' }
Plug 'vim-ruby/vim-ruby',        { 'for': 'ruby' }
Plug 'ngmy/vim-rubocop',         { 'for': 'ruby' }
Plug 'jgdavey/vim-blockle',      { 'for': 'ruby' }
" ruby/rails related stuff }}

Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'

Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'kchmck/vim-coffee-script'

" color schemes
Plug 'altercation/vim-colors-solarized'

" search tool
Plug 'rking/ag.vim'

Plug 'vim-airline/vim-airline'

Plug 'tpope/vim-commentary'
Plug 'scrooloose/nerdtree'

" Better whitespace highlighting for Vim
Plug 'ntpeters/vim-better-whitespace'

" Linters support
Plug 'benekastah/neomake'

Plug 'tpope/vim-fugitive'
Plug 'mattn/emmet-vim'

Plug 'Raimondi/delimitMate'

" jsx syntax
Plug 'mxw/vim-jsx'

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" Deoplete sources for ruby language
Plug 'fishbullet/deoplete-ruby'

Plug 'carlitux/deoplete-ternjs'

" Seamless navigation between tmux panes and vim splits
Plug 'christoomey/vim-tmux-navigator'

" vim plugin for tmux.conf
Plug 'tmux-plugins/vim-tmux'

" A Vim plugin which shows a git diff in the gutter (sign column)
" and stages/undoes hunks.
Plug 'airblade/vim-gitgutter'
" List ends here. Plugins become visible to Vim after this call.
"
Plug 'chip/vim-fat-finger'

Plug 'EinfachToll/DidYouMean'

Plug 'metakirby5/codi.vim'
call plug#end()

autocmd! BufWritePost * Neomake
let g:neomake_javascript_enabled_makers = ['eslint']
let g:neomake_ruby_enabled_makers = ['rubocop']

let g:jsx_ext_required = 0

let g:python_host_prog = '/usr/local/bin/python2'
let g:python3_host_prog = '/usr/local/bin/python3'

" configure leader key
let mapleader=","

set background=dark
colorscheme solarized

nnoremap <leader>sv :source $MYVIMRC<CR>
nnoremap <leader>ev :e $MYVIMRC<CR>

inoremap jj <Esc>

set number " add line numbers
set relativenumber
set ruler
set colorcolumn=81

"key mapping for tab navigation
nmap <Tab> gt
nmap <S-Tab> gT

" Strip trailing whitespace
autocmd BufWritePre * StripWhitespace

set clipboard=unnamed

nmap <Leader>p :NERDTreeToggle<CR>

" ag bindings
nmap <leader>g :Ag<space>
nnoremap K :Ag "\b<C-R><C-W>\b"<CR>:cw<CR>

" Test mappings
let test#strategy = "vimux"
nmap <silent> <leader>n :TestNearest<CR>
nmap <silent> <leader>s :TestFile<CR>
"nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>a :VimuxRunCommand("clear; echo 'running all specs...';bundle exec rake parallel:spec")<CR>
nmap <silent> <leader>f :VimuxRunCommand("clear; echo 'running only failures...';bundle exec rspec --only-failures")<CR>
nmap <silent> <leader>l :VimuxRunLastCommand<CR>
nmap <silent> <leader>g :TestVisit<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

let g:rails_projections = {
\ "config/projections.json": {
\   "command": "projections"
\ },
\ "app/services/*.rb": {
\   "command": "service",
\   "affinity": "model",
\   "test": "spec/services/%s_spec.rb",
\   "related": "app/models/%s.rb",
\   "template": "class %S\n\n  def self.call\n  end\nend"
\ },
\ "app/authorizers/*.rb": {
\   "command": "authorizer",
\   "affinity": "model",
\   "test": "spec/authorizers/%s_spec.rb"
\ },
\ "app/presenters/*_presenter.rb": {
\   "command": "presenter",
\   "affinity": "model",
\   "test": "spec/presenters/%s_spec.rb",
\   "related": "app/presenters/%s.rb",
\   "template": "class %SPresenter < BasePresenter\ndef initialize\n  end\nend"
\ },
\ "spec/factories/*.rb": {
\   "command":   "factory",
\   "affinity":  "collection",
\   "alternate": "app/models/%i.rb",
\   "related":   "db/schema.rb#%s",
\   "test":      "spec/models/%i_spec.rb",
\   "template":  "FactoryGirl.define do\n  factory :%i do\n  end\nend",
\   "keywords":  "factory sequence"
\},
\ "spec/features/*_spec.rb": {
\   "command":   "feature",
\   "template":  "require 'rails_helper'\n\nRSpec.describe '%s', type: :feature do\nend",
\},
\ "app/importers/*_importer.rb": {
\   "command": "importer",
\   "affinity": "model",
\   "test": "spec/importers/%s_spec.rb",
\   "template": "class %SImporter\n  def initialize\n  end\nend"
\ },
\ "app/processors/*_processor.rb": {
\   "command": "processor",
\   "test": "spec/processors/%s_spec.rb",
\   "template": "class %SProcessor\n  def initialize\n  end\nend"
\ }
\}

command! -nargs=* -complete=file -bang Rename :call Rename("<args>", "<bang>")
function! Rename(name, bang)
  let l:curfile = expand("%:p")
  let v:errmsg = ""
  silent! exe "saveas" . a:bang . " " . a:name
  if v:errmsg =~# '^$\|^E329'
    if expand("%:p") !=# l:curfile && filewritable(expand("%:p"))
      silent exe "bwipe! " . l:curfile
      if delete(l:curfile)
        echoerr "Could not delete " . l:curfile
      endif
    endif
  else
    echoerr v:errmsg
  endif
endfunction

" ag bindings
nmap <leader>g :Ag<space>
nnoremap K :Ag "\b<C-R><C-W>\b"<CR>:cw<CR>

" run set test lib
nnoremap <silent> <leader>rt :call neoterm#test#run('all')<cr>
nnoremap <silent> <leader>rf :call neoterm#test#run('file')<cr>
nnoremap <silent> <leader>rn :call neoterm#test#run('current')<cr>
nnoremap <silent> <leader>rr :call neoterm#test#rerun()<cr>

function! VimuxRunInspectCommand(args)
  call VimuxRunCommand(a:args)
  call VimuxInspectRunner()
endfunction

nnoremap <C-h> <C-w>h " only C-h does not work.
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
set tabstop=2 shiftwidth=2 expandtab

let g:user_emmet_settings = {
\    "html": {
\        "quote_char": "'"
\    },
\    "javascript.jsx": {
\        "extends": "jsx",
\        "quote_char": "'"
\    }
\}

map ,c :%s/"/'/g<CR>

let g:deoplete#enable_at_startup = 1
